#!/usr/bin/python3
from collections import Counter
#votelist is name of list of values of ballot.choice
#ballots is name of collection of ballots

def __init__(candidates)
	global candidates_list{}
	for candidate in candidates:
		candidates_list[candidate] = 0


class ballot:

	def __init__(self,choice):
		#choice here is an int, each candidate has a number
		self.choice = choice
	def add_to_counting_dictionary(self):
		if ballot.choice in candidates_list:
			candidates_list[ballot.choice]+=1
	@staticmethod
	def return_highest_two(self):
		top_two=[sorted(candidates_list.values())[-1], sorted(candidates_list.values())[-2]]
		#check candidates list for these values then make a new dictionary of only these two
		keys = ([k for k, v in candidates_list.items() if v == top_two[0]],[k for k, v in candidates_list.items() if v == top_two[1]])
		winners = {}
		for i in range (0,1):
			if (i==0):
				winners["First place"] = candidates_list.get(keys[i])
			if (i==1):
				winners["Second place"] = candidates_list.get(keys[i])
