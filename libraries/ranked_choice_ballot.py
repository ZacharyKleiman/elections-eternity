#!/usr/bin/python3
from itertools import permutations
#votelist is name of list of values of ballot.choice
#ballots is name of collection of ballots

def __init__(candidates)
	global candidates_list{}
	for candidate in candidates:
		candidates_list[candidate] = 0
	global permutations_list=[]
	for cand_permutations in range (1,len(candidates_list)):
		permutations_list.append(itertools.permutations(candidates_list.keys(),cand_permutations))


class ballot:

	def __init__(self,choices[]):
		#choice here is an int, each candidate has a number
		self.choice[] = choices[]
	def add_to_counting_dictionary(self):
		if ballot.choices[] in permutations_list:
			permutations_list[ballot.choices]+=1
	@staticmethod
	def compute_winner(self):
	#data to use: Permutations_list contains lists of all possible combinations. Analyze the first element of the list and make a temp dictionary from that data. Then run the algorithm below. If no further choice eliminate that list from the superlist.  If yes then do the same for the 2nd (assuming the algorithm hasn't found a winner). Repeat until a winner is found
	#algorithm here is
	# 1. Check first value of each key. Add to a new dictionary first values of the lists of each key and combine similar values
	# 2. Check if any candidate exceeds 50%. If so terminate.
	# 3. Eliminate the lowest ranking value in the permutations dictionary
	# 4. If there's another value then move the votes to the next candidate represented by the integer in candidates_list
	# 5. If there isn't another value then just pop from the dictionary.
	#6. Back to 1
