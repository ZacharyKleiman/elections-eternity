#!/usr/bin/python3

from voting.apportionment import dhondt
from voting.apportionment import adams
from voting.apportionment import huntington_hill
from voting.apportionment import hamilton
from voting.apportionment import sainte_lague

def __init__(parties)
	global parties_list{}
	for party in parties:
		parties_list[party] = 0


class ballot:

	def __init__(self,choice):
		#choice here is an int, each candidate has a number
		self.choice = choice
	def add_to_counting_dictionary(self):
		if ballot.choice in parties_list:
			parties_list[ballot.choice]+=1
	@staticmethod
	def get_seats_jefferson(self,seats):
		list_of_votes=[]
		for values in parties_list:
			list_of_votes.append(values)
		list_of_seats[]=dhondt(list_of_votes,seats)
		parties_list_seats{}=parties_list
		for seats in parties_list_seats:
			parties_list_seats[seats]=list_of_seats[seats]
	@staticmethod
	def get_seats_adams(self,seats):
		list_of_votes=[]
		for values in parties_list:
			list_of_votes.append(values)
		list_of_seats[]=adams(list_of_votes,seats)
		parties_list_seats{}=parties_list
		for seats in parties_list_seats:
			parties_list_seats[seats]=list_of_seats[seats]
	@staticmethod
	def get_seats_hamilton(self,seats):
		list_of_votes=[]
		for values in parties_list:
			list_of_votes.append(values)
		list_of_seats[]=hamilton(list_of_votes,seats)
		parties_list_seats{}=parties_list
		for seats in parties_list_seats:
			parties_list_seats[seats]=list_of_seats[seats]
	@staticmethod
	def get_seats_huntington_hill(self,seats):
		list_of_votes=[]
		for values in parties_list:
			list_of_votes.append(values)
		list_of_seats[]=huntington_hill(list_of_votes,seats)
		parties_list_seats{}=parties_list
		for seats in parties_list_seats:
			parties_list_seats[seats]=list_of_seats[seats]
	@staticmethod
	def get_seats_sainte_lague(self,seats):
		list_of_votes=[]
		for values in parties_list:
			list_of_votes.append(values)
		list_of_seats[]=sainte_lague(list_of_votes,seats)
		parties_list_seats{}=parties_list
		for seats in parties_list_seats:
			parties_list_seats[seats]=list_of_seats[seats]

	@staticmethod
	#fix up the results by removing parties that don't meet the threshold
	def nuke_no_threshold(self,threshold)
		for value in parties_list:
			if (parties_list[value]/(sum(parties_list.values())) < threshold):
				parties_list.pop(value)
