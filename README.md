# Elections Eternity

## Description
This feature will let you do realistic playthroughs of elections both modern and historical. It's meant to be a proper election simulator so the audience is more towards people who like realistic simulation games.

## Support
Open up an issue in the issue section.
## Roadmap

Stage 1 - Finish Developing Libraries to handle voting (in progress)

Stage 2 - Work on Libraries to Handle the .elect files in game

Stage 3 - prototype a text based version of a small election using these libraries to iron out bugs

Stage 4 - Work on the UI of the game

Stage 5 - Develop campaigns for the game based on Historical Elections

## Historical Elections planned for inclusion in the first release
(If you're a a historian or otherwise knowledgeable in the history of these elections we want your help).
- German 1933 Federal Election
- January 1907 Russian Duma Election
- 1871 Japanese general Election
- United Kingdom 1945 Election
- United States 1860 Election
## Contributing

If you know Python and are interested in Election simulation software we can use your help. Also, if you're a historian or are otherwise knowledgeable in history you're expertise for designing campaigns will be greatly appreciated.
## Authors and acknowledgment
Zachary Kleiman
## License
GPL for now
## Project status
Development started
